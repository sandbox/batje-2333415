<?php
/**
 * @file
 * Defines a Charts Graphs field and formatter 
 *
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
* Implements hook_field_info().
*
* Provides additional fields for charts_graphs_field.
*/
function charts_graphs_field_field_info() {
  return array(
  // We name our field as the associative name of the array.
    'charts_graphs_field' => array(
      'label' => t('Charts Graphs Field'),
      'description' => t('A field that shows Charts Graphs.'),
      'default_widget' => 'number',
      'default_formatter' => 'charts_graphs_formatter',
      'property_type' => 'integer', // we add the property type here
      'property_callbacks' => array('charts_graphs_field_responsecount_property_info_callback'), // we add the callback
      'instance_settings' => array('prefix' => '', 'suffix' => ''),
      'no_ui' => TRUE,
    ),
  );
}

/**
 * Implements hook_entity_property_info_alter.
 */
function charts_graphs_field_entity_property_info_alter_lalala(&$info) {
  $properties = &$info['node']['bundles']['charts_graphs_field']['properties'];
  $properties['charts_graphs_field_responsecount'] = array(
    'label' => t('Response Count'),
    'description' => t('The number of responses in the system for this question.'),
    'type' => 'integer',
    'getter callback' => 'charts_graphs_field_responsecount_getter_callback',
    'computed' => TRUE,
    'entity views field' => TRUE,
    'entity token' => TRUE,
  );
}

/**
 * Callback function with additional information about the responsecount field
 */
function charts_graphs_field_responsecount_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $properties = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$field['field_name']];
  $properties['label'] = t('Response Count');
  $properties['description'] = t('The number of responses in the system for this question.');
  $properties['type'] = 'integer';
  $properties['getter callback'] = 'charts_graphs_field_responsecount_getter_callback';
  $properties['computed'] = TRUE;
  $properties['entity views field'] = TRUE;
  $properties['entity token'] = TRUE;
}

/**
 * Getter for the responsecount attribute on a questionaire_question node
 */
function charts_graphs_field_responsecount_getter_callback($node) {
  $value = db_query("SELECT COUNT(id) as cnt FROM {questionnaire_answer} WHERE question = :qid", array('qid'=> $node->nid))->fetchField();
  return $value;
  /*  switch ($node->charts_graphs_field_type[$node->language][0]['value']) {
    case "radios":
    case "select":
    case "checkboxes" :
      $value = db_query("SELECT COUNT(id) as cnt FROM {questionnaire_answer} WHERE question = :qid GROUP BY answertext", array('qid'=> $node->nid))->fetchField();
      return $value;
      break;
    break;
    case "number" :
      $value = db_query("SELECT COUNT(id) as cnt FROM {questionnaire_answer} WHERE question = :qid GROUP BY answertext", array('qid'=> $node->nid))->fetchField();
      return $value;
      break;
    case "message":
      break;
  }*/
}

/**
 * Adds Response Count attribute to core node
 *
 * Implements hook_node_load()
 */
function charts_graphs_field_node_load($nodes, $types) {
  foreach ($nodes as $node) {
    if ($node->type == 'charts_graphs_field') {
      $node->qq_responsecount[$node->language][0]['value'] = charts_graphs_field_responsecount_getter_callback($node);
    }
  }
}

/*
 * Implements hook_field_is_empty()
 */
function charts_graphs_field_field_is_empty($item, $field) {
  // Even if the field were empty, that would be 0
  return FALSE;
}

/**
 * From here we define the formatter
 */



/**
 * Implements hook_field_formatter_info().
 */
function charts_graphs_field_formatter_info() {
  return array(
    'charts_graphs_formatter' => array( //Machine name of the formatter
      'label' => t('Charts & Graphs'),
      'field types' => array('number_integer', 'number_decimal', 'number_float'), //This will only be available to text fields
      'settings'  => array( //Array of the settings we'll create
        'graphs' => array (
            'width' => '',
            'height' => '',
            'y_min' => '',
            'y_max' => '',
            'y_step' => '',
            'y_legend' => '',
            'background_colour' => '',
            'series_colours' => '',
            'showlegend' => '',
            'showzoom' => '',
          ),
        ),
      ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function charts_graphs_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
    //This gets the view_mode where our settings are stored
  $display = $instance['display'][$view_mode];
  //This gets the actual settings
  $settings = $display['settings'];
  //Initialize the element variable
  $element = array();


  $element['graphs'] = array(
      '#type' => 'fieldset',
      '#title' => t('Graph Settings'),
      '#description' => t('Settings for the Graph.'),
      '#weight' => 0,
    );

  $element['graphs']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Canvas width'),
      '#description' => t('Canvas width, for libraries that support it you can supply a percentage, otherwise, provide a number for pixels. Defaults to 600px when input is invalid.'),
      '#default_value' => $settings['graphs']['width'],
    );

  $element['graphs']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Canvas height'),
      '#description' => t('Canvas height in pixels only.  Defaults to 500px when input is invalid.'),
      '#default_value' => $settings['graphs']['height'],
    );

    $engines = array();
    $types = array();
    $api_names = array();

    foreach ($apis = charts_graphs_apis() as $api) {
      $engines[$api->name] = $api->nice_name;
      $types[$api->name] = $api->chart_types;
      $api_names[] = $api->name;
    }

    sort($api_names);

  $element['graphs']['engine'] = array(
      '#type' => 'select',
      '#title' => t('Charting Engine'),
      '#options' => $engines,
      '#default_value' => $settings['graphs']['engine'],
    );

    $current_engine = empty($settings['graphs']['engine']) ?
      $api_names[0] :
      $settings['graphs']['engine'];

    foreach ($types as $engine => $type) {
      $default = !empty($settings['graphs'][$engine]) ?
        $settings['graphs'][$engine] :
        $default = array_shift(array_keys($type)
      );
      $hidden = NULL;

      if ($engine != $current_engine) {
        $hidden = 'facetapi_graphs_chart_types_hidden';
      }

    $element['graphs'][$engine] = array(
        '#type' => 'radios',
        '#title' => t('Chart Type'),
        '#options' => $type,
        '#required' => TRUE,
        '#default_value' => $default,
        '#prefix' => sprintf(
          '<div class="facetapi_graphs_chart_types facetapi_graphs_%s_chart_types %s">',
          $engine,
          $hidden
        ),
        '#suffix' => '</div>',
      );
    }

  $element['graphs']['y_min'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimun value of Y Axis'),
      '#default_value' => ($settings['graphs']['y_min']) ? $settings['graphs']['y_min'] : '',
    );

  $element['graphs']['y_max'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximun value of Y Axis'),
      '#default_value' => ($settings['graphs']['y_max']) ? $settings['graphs']['y_max'] : '',
    );

  $element['graphs']['y_step'] = array(
      '#type' => 'textfield',
      '#title' => t('Step value of Y Axis'),
      '#default_value' => ($settings['graphs']['y_step']) ? $settings['graphs']['y_step'] : '',
    );

  $element['graphs']['y_legend'] = array(
      '#type' => 'textfield',
      '#title' => t('Y Legend'),
      '#default_value' => $settings['graphs']['y_legend'],
    );

  $element['graphs']['background_colour'] = array(
      '#type' => 'textfield',
      '#title' => t('Graph background colour'),
      '#description' => t('Define the colour in hexadecimal: #RRGGBB'),
      '#default_value' => $settings['graphs']['background_colour'],
    );

  $element['graphs']['series_colours'] = array(
      '#type' => 'textfield',
      '#title' => t('Series colours'),
      '#description' => t('Define the colour of each series as a comma separated list of hexadecimal colour definitions. Ex: #123456,#654321,#1177ff'),
      '#default_value' => $settings['graphs']['series_colours'],
    );

  $element['graphs']['showlegend'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Legend'),
      '#description' => t('Show or hide the legend, if the library supports it.'),
      '#default_value' => $settings['graphs']['showlegend'],
    );

  $element['graphs']['showzoom'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Zoom (flot only)'),
      '#description' => t('Show or hide the zoom, if the library supports it.'),
      '#default_value' => $settings['graphs']['showzoom'],
    );


  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function charts_graphs_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = t('Use a @engine @graph Graph', array(
    '@engine'     => $settings['graphs']['engine'],
    '@graph'  => $settings['graphs'][$settings['graphs']['engine']],
  )); // we use t() for translation and placeholders to guard against attacks
  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function charts_graphs_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array(); // Initialize the var
  $settings = $display['settings']; // get the settings
  $attempts = array("good", "average", "bad", "aweful", "horrible");

  $durations = array (
    "a" => 100, 
    "b" => 200, 
    "c" => 400, 
    "d" => 600, 
    "e" => 330);

  $canvas = charts_graphs_get_graph($settings['graphs']['engine']);

  $canvas->title = "Bluff Chart";
  $canvas->type = $settings['graphs'][$settings['graphs']['engine']]; // a chart type supported by the charting engine. 
  $canvas->y_legend = "Durations";
  $canvas->colour = '#D54C78';
  $canvas->theme = 'keynote';
  $canvas->width = $settings['graphs']['width'];
  $canvas->height = $settings['graphs']['height'];
  
  $canvas->y_min = $settings['graphs']['y_min'];
  $canvas->series = array(
  //      'times' => $times,
      'durations' => $durations,
  );
  $canvas->x_labels = $attempts;

  $out = $canvas->get_chart();

  
  $element[0] = $out; // Assign it to the #markup of the element
  return $element;
}

/**
 * Implements hook_field_formatter_info_alter()
 */
function charts_graphs_field_field_formatter_info_alter(&$info) {
  // Let a new field type re-use an existing formatter.
  $info['number_integer']['field types'][] = 'charts_graphs_formatter';
}